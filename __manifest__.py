# -*- coding: utf-8 -*-
{
    'name': 'Product Code Generation',
    'version': '13.0.1.0.0',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        'product',
    ],
    'data': [
        'data/ir_actions_server.xml',
        'data/ir_sequence.xml',
    ],
}
