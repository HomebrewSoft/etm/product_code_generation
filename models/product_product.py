# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class ProductProduct(models.Model):
    _inherit = 'product.product'

    default_code = fields.Char(
        readonly=True,
    )

    @api.model
    def update_with_product_code(self):
        for product in self.search([('default_code', '=', False)]):
            product.default_code = self.env['ir.sequence'].next_by_code('product.variant.code') or _('New')

    @api.model
    def create(self, vals):
        vals['default_code'] = self.env['ir.sequence'].next_by_code('product.variant.code') or _('New')
        result = super(ProductProduct, self).create(vals)
        return result
